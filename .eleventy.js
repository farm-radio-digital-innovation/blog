const moment = require('moment');
const pluginSass = require("eleventy-plugin-sass");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
 
moment.locale('en');
 
module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(pluginSass, {});
  eleventyConfig.addPlugin(syntaxHighlight, {
    templateFormats: ["*"], 
    alwaysWrapLineHighlights: false,
    trim: true,
    lineSeparator: "<br>",
  });

  eleventyConfig.addPassthroughCopy('css', () => {
    passthroughFileCopy: true
  })

  eleventyConfig.addPassthroughCopy('img', () => {
    passthroughFileCopy: true
  })

  eleventyConfig.addFilter('dateIso', date => {
    return moment(date).toISOString();
  });
 
  eleventyConfig.addFilter('dateReadable', date => {
    return moment(date).utc().format('LL'); // E.g. May 31, 2019
  });
};
